package org.academiadecodigo.sshpecials.game;

public class Hunter {

    private Shotgun shotgun;


    public Hunter() {
       this.shotgun = shotgun;
    }

    public void setShotgun(Shotgun shotgun) {
        this.shotgun = shotgun;
    }

    public Shotgun getShotgun() {
        return shotgun;
    }

    // I want my Hunter to hunt hares
// He can shoot at them, using a Shotgun
// He can set the Hound to chase them
}
