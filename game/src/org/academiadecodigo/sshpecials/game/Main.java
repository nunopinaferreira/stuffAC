package org.academiadecodigo.sshpecials.game;

public class Main {

    public static void main(String[] args) {


        Shotgun shotgun = ShotgunFactory.createShotgun();
        Hunter hunter = new Hunter();
        hunter.setShotgun(shotgun);
        Hare bugsbunny = new Hare();

        hunter.getShotgun().shoot(bugsbunny);



    }

}
